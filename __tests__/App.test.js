import firebase from './../src/database/firebase'

//Prueba login 
it("Login", async () => {
  //Credenciales para el login
  var email = "ariasdiego1160@gmail.com";
  var pin = 123456;
  try {
    //busqueda del usuario en la nube con las credenciales correspondientes
    const query = await firebase.db.collection('usuarios').where('email', '==', email).get();
    //Verificación de si se encontro la cuenta del usuario
    if (!query.empty) {
      //Busqueda de correo y contraseña validado mediante Firebase Authentication para facilitar la busqueda de sistemas de autenticación seguros
      var Usuario = await firebase.auth.signInWithEmailAndPassword(email, pin);
      //mediante Firebase Authentication se busca el email verificado
      if (Usuario.user.emailVerified == true) {
        console.log("Login exitoso");
        expect(Usuario.user.email).toBe(email);
      } else {
        console.log("Login no verficado")
      }
    } else {
      console.log("Login fallido")
    }
  } catch (e) {
    console.log(e);
  }
});

//Prueba registro
it("Registro", async () => {
  //datos para la creacion de una cuenta nueva
  var nombre = "Diego Armando";
  var apellido1 = "Arias";
  var apellido2 = "Álvarez";
  var telefono = 4421260920;
  var email = "ariasdiegoarmando1160@gmail.com";
  var pin = 123456;

  try {
    //Si la cuenta es existente automaticamente paso la prueba
    const query = firebase.db.collection('usuarios').where('email', '==', email).get();
    if (!query.empty) {
      console.log("Registro ya hecho");
    //De lo contrario se realizara la función del registro
    } else {
      //Se crea mediante Firebase Authentication un nuevo usuario donde se solicitara mediante email la validación del correo
      const newUsuario = await firebase.auth.createUserWithEmailAndPassword(email, pin);
      await newUsuario.user.sendEmailVerification();

      //Se proceden a guardar los datos en la collection correspondiente en Firebase Storage
      const registro = firebase.db.collection("usuarios").add({
        authId: usuarioFirebase.user.uid,
        nombre: nombre,
        apellido1: apellido1,
        apellido2: apellido2,
        telefono: telefono,
        QRentrada:
          'https://firebasestorage.googleapis.com/v0/b/rowait-96ed3.appspot.com/o/QR%2Fentrada.png?alt=media&token=51d61965-b275-4fae-96d7-423b9a34fe22',
        QRsalida:
          'https://firebasestorage.googleapis.com/v0/b/rowait-96ed3.appspot.com/o/QR%2Fsalida.png?alt=media&token=ea0e83b3-c6f7-411c-b2c9-4ba2faa02e66',
        email: email,
        estatus: 'cliente',
      });
      console.log("Registro completado");
    }
  } catch (e) {
    console.log(e);
  }
});

//Prueba plazas
it("Plazas", async () =>{
  try {
    //Busqueda general de las plazas
      let plaza = firebase.db.collection('plazas');
      let plazaData = await plaza.get()

      //Se muestra la información de las plazas
      for(const doc of plazaData.docs){
        console.log(doc.id, '=>', doc.data());
      }
  } catch (error) {
    console.log(error);
  }
});

it("Plaza", async () => {
  let nombrePlaza = "Las americas";
  try {
    //Busqueda especifica de una plaza mediante el nombre de la plaza
      let plaza = firebase.db.collection('plazas').where('nombre', '==', nombrePlaza);
      let plazaData = await plaza.get()

      //Se muestra la informacion de la plaza
      for(const doc of plazaData.docs){
        console.log(doc.id, '=>', doc.data());
        expect(doc.data().nombre).toBe(nombrePlaza);
      }
  } catch (error) {
    console.log(error);
  }
});