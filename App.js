import React, { useEffect } from 'react';
import { LogBox } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

//Views Publicas
import Login from './src/screens/Login';
import Inicio from './src/screens/Inicio';
import Registro from './src/screens/Registro';

//Views Privadas
import Home from './src/screens/private/Home';
import Plazas from './src/screens/private/Plazas';
import QrEntrada from './src/screens/private/QrEntrada';
import QrSalida from './src/screens/private/QrSalida';
import Maps from './src/screens/private/Maps';
import Empleado from './src/screens/private/Empleado';

export default function App() {
  useEffect(() => {
    LogBox.ignoreLogs([
      'Animated: `useNativeDriver`',
      'Setting a timer for a long period of time',
    ]);
  }, []);

  useEffect(() => {
    /**
     * Indicamos los tipos de warnig que queremos dejar de visualizar
     *
     */

    //TODOS
    LogBox.ignoreAllLogs();
    //Indicar
    //LogBox.ignoreLogs(['Animated:`useNativeDrive`']);
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen options={{ headerTitle: 'Inicio', headerShown: true }} name='Inicio' component={Inicio} />

        <Stack.Screen name='Login' component={Login} />

        <Stack.Screen name='Registro' component={Registro} />

        <Stack.Screen name='Home' component={Home} />

        <Stack.Screen name='Plazas' component={Plazas} />

        <Stack.Screen name='Maps' component={Maps} />

        <Stack.Screen name='Empleado' component={Empleado} />

        <Stack.Screen name='QrEntrada' component={QrEntrada} />

        <Stack.Screen name='QrSalida' component={QrSalida} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

