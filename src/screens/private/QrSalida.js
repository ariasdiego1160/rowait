import React, { useEffect, useState } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import estilos from '../../styles/estilos';
import { Avatar, Button, Card } from 'react-native-paper';
import firebase from './../../database/firebase';

const styles = StyleSheet.create({
  logo: {
    width: 350,
    height: 350,
  },
});

const LeftContent = (props) => <Avatar.Icon {...props} icon='qrcode' />;

const QrSalida = (props) => {
  const [usuarioFirebase, setUsuarioFirebase] = useState({});
  const [docUsuario, setDocUsuario] = useState({});
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    /* tomamos los datos del usuario que ha iniciado sesión */
    setUsuarioFirebase(firebase.auth.currentUser);

    /* invocamos la consulta */
    getDocUsuario(firebase.auth.currentUser.uid);
  }, []);

  /**
   * Función flecha que ejecuta una consulta sobre la colección
   * usuarios
   */
  const getDocUsuario = async (uid) => {
    try {
      const query = await firebase.db
        .collection('usuarios')
        .where('authId', '==', uid)
        .get();

      /**
       * Si la consulta no esta vacía
       */
      if (!query.empty) {
        /* cuando esperamos solo un registro */
        const snapshot = query.docs[0];

        setDocUsuario({
          ...snapshot.data(),
          id: snapshot.id,
        });

        setLoading(false);
      }
    } catch (e) {
      console.warn(e.toString());
    }
  };

  return (
    <Card>
      <View
        style={{
          alignItems: 'center',
        }}
      >
        <Card.Title
          title='Codigo QR de salida'
          subtitle='Para salir de la plaza mostrar este codigo QR.'
          left={LeftContent}
        />
        <Image
          source={{ uri: docUsuario.QRsalida }}
          style={(estilos.imgRegis, styles.logo)}
        />
      </View>
    </Card>
  );
};

export default QrSalida;
