import React, { useEffect, useState } from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import { Avatar, Button, Card } from 'react-native-paper';
import { AntDesign, MaterialIcons } from '@expo/vector-icons';

const LeftContent = (props) => <Avatar.Icon {...props} icon='qrcode' />;

const MiQr = (props) => (
  <Card>
    <Card.Title
      title='Codigo QR'
      subtitle='Dar clic en el boton para ver el codigo entrada o salida.'
      left={LeftContent}
    />

    <Card.Cover
      source={{ uri: 'https://i.blogs.es/7a38da/codigos-qr/1366_2000.jpg' }}
    />
    <View
      style={{
        alignItems: 'center',
      }}
    >
      <Card.Actions>
        <Button
          mode='contained'
          onPress={() => {
            props.navigation.navigate('QrEntrada');
          }}
        >
          <AntDesign name='plus' size={20} color='#ffff' />
          {'  '}
          QR entrada
        </Button>
        <Text>{'   '}</Text>
        <Button
          mode='contained'
          onPress={() => {
            props.navigation.navigate('QrSalida');
          }}
        >
          <AntDesign name='minus' size={20} color='#ffff' />
          {'  '}
          QR salida
        </Button>
      </Card.Actions>
    </View>
  </Card>
);

export default MiQr;
