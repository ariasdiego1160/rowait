import React, { useState, useEffect } from 'react';
import { Alert, SafeAreaView, Text, View } from 'react-native';
import ProgressDialog from '../../components/ProgressDialog';
import firebase from '../../database/firebase';

/*
Para mostrar un mapa con el proveedor defecto
usamos un componente de tipo MapView
desde react-native-maps
*/
import MapView, { Marker, Callout, PROVIDER_GOOGLE } from 'react-native-maps';

const Maps = ({ dato, route }) => {
  const [progress, setProgress] = useState(false);
  const [mapa, setMapa] = useState(null);
  const [plazas, setPlazas] = useState([]);

  //console.log(dato.params.Nombre);

  useEffect(() => {
    const arrPlazas = [];
    firebase.db
      .collection('plazas')
      .where('nombre', '==', route.params.Nombre)
      .onSnapshot((querySnapshot) => {
        querySnapshot.docs.map((doc) => {
          arrPlazas.push({
            ...doc.data(),
            id: doc.id,
          });
        });
        setPlazas(arrPlazas);
      });
    //console.log(plazas[0]);
  }, []);

  //console.log(plazas);

  /*
    Función flecha que pida permiso de ir por la ubicación 
    (latitud y longitud del usuario)
    */
  if (plazas.length == 0) {
    return null;
  }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      {progress && <ProgressDialog />}

      <MapView
        /* Creamos una referencia del mapa para poder utilizarlo
                fuera de MapView*/
        provider={PROVIDER_GOOGLE}
        showsUserLocation
        followsUserLocation
        style={{
          flex: 8,
        }}
        initialRegion={{
          latitude: plazas[0].latitud,
          longitude: plazas[0].longitud,
          latitudeDelta: 0.1,
          longitudeDelta: 0.1,
        }}
      >
        <Marker
          coordinate={{
            latitude: plazas[0].latitud,
            longitude: plazas[0].longitud,
          }}
        >
          <Callout>
            <View style={{ padding: 10 }}>
              <Text
                style={{
                  fontSize: 18,
                  marginBottom: 10,
                }}
              >
                {plazas[0].nombre}
              </Text>

              <Text>{plazas[0].direccion}</Text>
              <Text>Telefono: {plazas[0].telefono}</Text>
            </View>
          </Callout>
        </Marker>
      </MapView>
    </SafeAreaView>
  );
};

export default Maps;
