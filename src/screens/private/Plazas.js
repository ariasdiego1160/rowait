import React, { useEffect, useLayoutEffect, useState } from 'react';
import firebase from './../../database/firebase';
import { View, SafeAreaView, FlatList, Image, ScrollView } from 'react-native';
import { Entypo, AntDesign } from '@expo/vector-icons';
import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  IconButton,
  Text,
  Banner,
} from 'react-native-paper';
import { useNavigation } from '@react-navigation/core';

/* es una referencia a las variables, const, obj, componentes, etc
 * que comparte el componente padre conmigo
 */
const Plazas = ({ route }) => {
  const [plazas, setPlazas] = useState([]);
  const [visible, setVisible] = React.useState(true);
  const navigation = useNavigation();

  useEffect(() => {
    const arrPlazas = [];
    firebase.db
      .collection('plazas')
      .where('nombre', '==', route.params.Plaza)
      .onSnapshot((querySnapshot) => {
        querySnapshot.docs.map((doc) => {
          arrPlazas.push({
            ...doc.data(),
            id: doc.id,
          });
        });
        setPlazas(arrPlazas);
      });
    //console.log(plazas[0]);
  }, []);

  //console.log(plazas[0].nombre);

  return (
    <ScrollView>
      <SafeAreaView>
        <Banner
          visible={visible}
          actions={[
            {
              label: 'Cancelar',
              onPress: () => setVisible(false),
            },
            {
              label: 'visitar',
              onPress: () =>
                navigation.navigate('Maps', { Nombre: plazas[0].nombre }),
            },
          ]}
          icon={({ size }) => (
            <Image
              source={require('./../../../assets/images/marcador.png')}
              style={{
                width: 35,
                height: 35,
              }}
            />
          )}
        >
          ¿Quieres visualizar el mapa?
        </Banner>

        <FlatList
          style={{ margin: 15 }}
          data={plazas}
          keyExtractor={(item) => item.id}
          renderItem={(item) => (
            <View style={{ marginVertical: 10 }}>
              <Card>
                <Card.Cover source={{ uri: item.item.imagen }} />

                <Card.Content>
                  <Title style={{ alignItems: 'center', marginVertical: 10 }}>
                    {item.item.nombre}
                  </Title>
                  <Paragraph style={{ color: '#4F289D' }}>Dirección:</Paragraph>
                  <Paragraph>{item.item.direccion}</Paragraph>
                  <Paragraph style={{ color: '#4F289D' }}>Telefono:</Paragraph>
                  <Paragraph>{item.item.telefono}</Paragraph>
                  <Paragraph style={{ color: '#4F289D' }}>
                    Poblacion actual:
                  </Paragraph>
                  <Text>{item.item.poblacionActual}</Text>
                  <Paragraph style={{ color: '#4F289D' }}>
                    Poblacion general:
                  </Paragraph>
                  <Paragraph>{item.item.poblacionGeneral}</Paragraph>
                </Card.Content>
              </Card>
            </View>
          )}
        />
      </SafeAreaView>
    </ScrollView>
  );
};

export default Plazas;
