import React, { useEffect, useState, useLayoutEffect } from 'react';
import { Alert, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { ActivityIndicator, Image, Text, View, ScrollView } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import firebase from './../database/firebase';
import estilos from '../styles/estilos';
import get_error from '../helpers/errores_es_mx';

const Login = (props) => {
  const [email, setEmail] = useState('');
  const [pin, setPin] = useState('');
  const [btnVisible, setBtnVisible] = useState(true);
  const [aiVisible, setAiVisible] = useState(false);
  const [tiHab, setTiHab] = useState(true);
  const [docUsuario, setDocUsuario] = useState({});

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          style={estilos.inicio}
          onPress={() => {
            props.navigation.navigate('Inicio');
          }}
        >
          <AntDesign name='home' size={20} />
        </TouchableOpacity>
      ),
    });
  }, []);

  //Funcion que valida los datos al momento de hacer login y que no sean erroneos
  const validaLogin = async () => {
    if (email.length < 5) {
      Alert.alert(
        'ERROR',
        'Email incorrecto',
        [
          {
            text: 'Corregir',
            onPress: () => {
              setEmail('');
            },
          },
        ],
        { cancelable: false }
      );
      return;
    }

    if (pin.length !== 6) {
      Alert.alert(
        'ERROR',
        'Pin incorrecto',
        [
          {
            text: 'Corregir',
            onPress: () => {
              setPin('');
            },
          },
        ],
        { cancelable: false }
      );

      return;
    }

    setAiVisible(true);
    setBtnVisible(false);
    setTiHab(false);

    try {
      console.log(email);
      //busqueda del usuario en la nube con las credenciales correspondientes
      const query = await firebase.db
        .collection('usuarios')
        .where('email', '==', email)
        .get();
      if (!query.empty) {
        const snapshot = query.docs[0];
        setDocUsuario({
          ...snapshot.data(),
          id: snapshot.id,
        });
      }
      //Busqueda de correo y contraseña validado mediante Firebase Authentication para facilitar la busqueda de sistemas de autenticación seguros
      const usuarioFirebase = await firebase.auth.signInWithEmailAndPassword(
        email,
        pin
      );

      console.log(docUsuario.estatus);
      let mensaje = `Bienvenido ${usuarioFirebase.user.email}\n`;
      mensaje += usuarioFirebase.user.emailVerified
        ? 'Usuario verificado'
        : ' Por favor valida tu cuenta ';

      Alert.alert('Hola de nuevo', mensaje, [
        {
          text: 'Ingresar',
          onPress: () => {
            setAiVisible(false);
            setBtnVisible(true);
            setTiHab(true);
            //Dependiendo del estatus del usuario se redireccionara a diferentes pantallas
            if (docUsuario.estatus == 'cliente') {
              props.navigation.navigate('Home');
            }
            if (docUsuario.estatus == 'empleado') {
              props.navigation.navigate('Empleado');
            }
          },
        },
      ]);
    } catch (e) {
      Alert.alert('ERROR', get_error(e.code), [
        {
          text: 'Corregir',
          onPress: () => {
            setAiVisible(false);
            setBtnVisible(true);
            setTiHab(true);
          },
        },
      ]);
    }
  };

  return (
    <ScrollView>
      <View>
        <View
          style={{
            alignItems: 'center',
            marginTop: 30,
          }}
        >
          <Image
            source={require('./../../assets/images/Logo.png')}
            style={estilos.imgLogin}
          />

          <Text style={estilos.titulo}>Iniciar sesión</Text>
        </View>

        <TextInput
          label='E-mail'
          keyboardType='email-address'
          maxLength={70}
          style={estilos.input}
          onChangeText={(val) => setEmail(val)}
          value={email}
          editable={tiHab}
        />

        <TextInput
          label='Pin (6 dígitos)'
          keyboardType='number-pad'
          secureTextEntry
          maxLength={6}
          style={estilos.input}
          onChangeText={(val) => setPin(val)}
          value={pin}
          editable={tiHab}
        />

        <View style={estilos.botones}>
          <Button mode='contained' onPress={validaLogin}>
            Continuar
          </Button>

          <Button
            mode='text'
            onPress={() => {
              props.navigation.navigate('Registro');
            }}
          >
            ¿No tienes una cuenta?
          </Button>
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;
